﻿using System;
using Xunit;
using System.Collections.Generic;
using QuadTree.Lib.Shapes;
using System.Numerics;
using FluentAssertions;

namespace QuadTree.Tests
{
    public class QuadTreeTests
    {
        [Fact]
        public void New_Quad_Has_NoSubtrees()
        {
            var bounds = new Rectangle(0, 0, 1, 1);
            var q = new Lib.Tree.QuadTree(bounds);

            q.HasSubtrees.Should().BeFalse("An empty quadtree node should have no subtrees");
        }

        [Fact]
        public void New_Quad_Bounds_Set()
        {
            var expected = new Rectangle(0, 0, 1, 1);
            var q = new Lib.Tree.QuadTree(expected);

            var actual = q.Bounds;
            actual.Should().Be(expected, "Bounds should not change");
        }

        [Fact]
        public void New_Quad_IsEmpty()
        {
            var bounds = new Circle(0, 0, 0.5f);
            var q = new Lib.Tree.QuadTree(new Rectangle(0, 0, 1, 1));

            var actual = q.Query(bounds, new List<Vector2>());
            actual.Should().BeEmpty("A new quadtree node should have no elements");
        }

        [Fact]
        public void Insert_InBounds_ReturnsTrue()
        {
            var q = new Lib.Tree.QuadTree(new Rectangle(0, 0, 1, 1));

            bool actualInsertion = q.Insert(new Vector2(0, 0));
            bool actualHasData = q.HasData;

            actualInsertion.Should().BeTrue("QuadTree should accept a point within its bounds");
            actualHasData
                .Should()
                .BeTrue("While within capacity, the node should hold the element");
        }

        [Fact]
        public void Insert_OutOfBounds_ReturnsFalse()
        {
            var q = new Lib.Tree.QuadTree(new Rectangle(0, 0, 1, 1));
            bool actualInsertion = q.Insert(new Vector2(5, 5));
            bool actualHasData = q.HasData;

            actualInsertion
                .Should()
                .BeFalse("Quadtree node should reject points outside its bounding region");
            actualHasData.Should().BeFalse("The out-of-bounds point should not be stored");
        }

        [Fact]
        public void DoesNot_Divide_AtCapacity()
        {
            var q = new Lib.Tree.QuadTree(new Rectangle(0, 0, 1, 1));
            // Insert points to bring the node to capacity
            for (var i = 0; i < q.Capacity; i++)
            {
                q.Insert(new Vector2(i / 2, 0));
            }

            bool actualData = q.HasData;
            bool actualSubtrees = q.HasSubtrees;

            actualSubtrees
                .Should()
                .BeFalse("Quadtree node should only divide when capacity is exceeded");
            actualData.Should().BeTrue("Quadtree node at capacity should hold the 4th element");
        }

        [Fact]
        public void Divided_OverCapacity_PointsDistributedToSubtrees()
        {
            var q = new Lib.Tree.QuadTree(new Rectangle(0, 0, 10, 10));
            for (var i = 0; i < q.Capacity + 1; i++)
            {
                q.Insert(new Vector2(i / 2, 0));
            }

            bool actualData = q.HasData;
            bool actualSubtrees = q.HasSubtrees;

            actualSubtrees.Should().BeTrue("Subtrees on this node should be created");
            actualData.Should().BeFalse("Elements should be moved down to the subtrees");
        }

        [Fact]
        public void Query_SingleItem()
        {
            var bounds = new Rectangle(0, 0, 1, 1);
            var q = new Lib.Tree.QuadTree(bounds);
            q.Insert(new Vector2(0, 0));

            ICollection<Vector2> actual = q.Query(new Circle(0, 0, 0.5f));

            actual.Should().ContainSingle("The node should hold a single element");
        }

        [Fact]
        public void Query_AllChildrenInSubTrees()
        {
            var bounds = new Rectangle(0, 0, width: 10, height: 10);
            var q = new Lib.Tree.QuadTree(bounds);
            var expectedCapacity = q.Capacity + 1;
            for (var i = 0; i < expectedCapacity; i++)
            {
                q.Insert(new Vector2(i / 2, 0));
            }

            ICollection<Vector2> actualFound = q.Query(new Circle(0, 0, radius: 10));
            int actualItems = actualFound.Count;

            actualFound.Should().NotBeEmpty("Should return all elements within its subtrees");
            actualItems
                .Should()
                .Be(expectedCapacity, "Should equal all elements from all subtrees");
        }
    }
}
