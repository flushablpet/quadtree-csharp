﻿using Xunit;
using QuadTree.Lib.Shapes;

using System.Numerics;
using System;
using QuadTree.Lib.Tree;

namespace QuadTree.Tests;

public class RectangleTests
{
    private static Rectangle CreateRectangle(float x, float y, float w, float h) => new(x, y, w, h);

    [Theory]
    [InlineData(1, 1)]
    [InlineData(2, 1)]
    [InlineData(1, 2)]
    public void Create_Width_Height_Set(float w, float h)
    {
        var r = CreateRectangle(0, 0, w, h);

        var actualW = r.Width;
        var actualH = r.Height;

        Assert.Equal(w, actualW);
        Assert.Equal(h, actualH);
    }

    [Theory]
    [InlineData(-1, 1)]
    [InlineData(0, 1)]
    [InlineData(0, 0)]
    [InlineData(1, 0)]
    [InlineData(1, -1)]
    [InlineData(-1, -1)]
    public void Create_ZeroOrLess_WidthOrHeight_Throws(float w, float h)
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => CreateRectangle(0, 0, w, h));
    }

    [Theory]
    [InlineData(-1, -1)]
    [InlineData(0, 0)]
    [InlineData(1, 1)]
    public void Left_Equals_XMinusHalfWidth(float x, float y)
    {
        const float W = 1;
        var r = CreateRectangle(x, y, W, 1);

        var expected = x - W / 2;
        var actual = r.Left;

        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData(-1, -1)]
    [InlineData(0, 0)]
    [InlineData(1, 1)]
    public void Right_Equals_XPlusHalfWidth(float x, float y)
    {
        const float W = 1;
        var r = CreateRectangle(x, y, W, 1);

        var expected = x + W / 2;
        var actual = r.Right;

        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData(-1, -1)]
    [InlineData(0, 0)]
    [InlineData(1, 1)]
    public void Top_Equals_YMinusHalfHeight(float x, float y)
    {
        const float H = 1;
        var r = CreateRectangle(x, y, w: 1, h: H);

        var expected = y - H / 2;
        var actual = r.Top;

        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData(-1, -1)]
    [InlineData(0, 0)]
    [InlineData(1, 1)]
    public void Bottom_Equals_YPlusHalfHeight(float x, float y)
    {
        const float H = 1;
        var r = CreateRectangle(x, y, w: 1, h: H);

        var expected = y + H / 2;
        var actual = r.Bottom;

        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData(-0.75, -0.75)]
    [InlineData(0, -1)]
    [InlineData(-1, 0)]
    [InlineData(0.5f, 1)]
    [InlineData(0, 1)]
    [InlineData(1, 1f)]
    public void DoesNot_Contain_Point_Outside(
        float x,
        float y,
        string msg = "Should not contain point"
    )
    {
        var r = CreateRectangle(0, 0, w: 1, h: 1);
        var actual = r.Contains(new Vector2(x, y));

        Assert.False(actual, msg);
    }

    [Theory]
    [InlineData(0, 0, "At origin")]
    [InlineData(0, 0.5, "On y perimeter")]
    [InlineData(0.5, 0, "On x perimeter")]
    [InlineData(0.5, 0.5, "SE corner")]
    [InlineData(-0.5, -0.5, "NW corner")]
    [InlineData(-0.5, 0.5, "NE corner")]
    [InlineData(0.5, -0.5, "SW corner")]
    public void Contains_Point(float x, float y, string msg)
    {
        var r = CreateRectangle(0, 0, w: 1, h: 1);
        var actual = r.Contains(new Vector2(x, y));

        Assert.True(actual, msg);
    }

    [Theory]
    [InlineData(1, 1, "On Perimeter")]
    [InlineData(0.5f, 0.5f, "Mid point")]
    [InlineData(0, 0, "Overlap")]
    public void Intersects(float x, float y, string msg)
    {
        var other = CreateRectangle(x, y, w: 1, h: 1);
        var r = CreateRectangle(0, 0, w: 1, h: 1);

        var actual = r.Intersects(other);

        Assert.True(actual, msg);
    }

    [Theory]
    [InlineData(0, 1.01f, "Below")]
    [InlineData(0, -1.01f, "Above")]
    [InlineData(-1.01f, 0, "Left")]
    [InlineData(1.01f, 0, "Right")]
    public void Does_Not_Intersect(float x, float y, string msg)
    {
        var other = CreateRectangle(x, y, w: 1, h: 1);
        var r = CreateRectangle(0, 0, w: 1, h: 1);

        var actual = r.Intersects(other);

        Assert.False(actual, msg);
    }

    [Theory]
    [InlineData(Quadrant.UPPER_LEFT, 0, 0)]
    [InlineData(Quadrant.UPPER_RIGHT, 0.5, 0)]
    [InlineData(Quadrant.BOTTOM_LEFT, 0, 0.5)]
    [InlineData(Quadrant.BOTTOM_RIGHT, 0.5, 0.5)]
    public void Subdivide(Quadrant q, float expectedX, float expectedY)
    {
        var r = CreateRectangle(0, 0, w: 1, h: 1);
        const float expectedW = 0.5f;
        const float expectedH = 0.5f;
        Vector2 expectedOrigin = new(expectedX, expectedY);

        var actual = r.Subdivide(q);

        Assert.Equal(expectedOrigin, actual.Origin);
        Assert.Equal(expectedW, actual.Width);
        Assert.Equal(expectedH, actual.Height);
    }
}
