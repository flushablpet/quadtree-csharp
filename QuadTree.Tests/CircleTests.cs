﻿using Xunit;
using QuadTree.Lib.Shapes;
using QuadTree.Lib.Tree;
using System.Numerics;
using System;
using FluentAssertions;

namespace QuadTree.Tests
{
    public class CircleTests
    {
        private static Circle CreateCircle(float x, float y, float r) => new(x, y, r);

        [Fact]
        public void Create_Origin_Set()
        {
            var c = CreateCircle(0, 0, 2);

            var expectedOrigin = new Vector2(0, 0);
            var actualOrigin = c.Origin;

            Assert.Equal(expectedOrigin, actualOrigin);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(4)]
        public void Create_Radius_Set(float r)
        {
            var c = CreateCircle(0, 0, r);

            var expected = r;
            var actualR = c.Radius;
            var actualRSq = c.RadiusSq;

            Assert.Equal(expected, actualR);
            Assert.Equal(expected * expected, actualRSq);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Create_ZeroOrLess_Radius_Throws(float r)
        {
            Assert.Throws<ArgumentException>(() => CreateCircle(0, 0, r));
        }

        [Theory]
        [InlineData(0, 0, "At origin")]
        [InlineData(0, 1, "At perimeter")]
        [InlineData(0.5f, 0.5f, "At r/2")]
        public void Contains_Point_WithinPermimeter_IsTrue(float x, float y, string msg)
        {
            var c = CreateCircle(0, 0, 1);

            var actual = c.Contains(new Vector2(x, y));

            Assert.True(actual, msg);
        }

        [Theory]
        [InlineData(1, 1, "At corner point")]
        [InlineData(-1, -1, "Neg corner point")]
        public void Contain_Point_OutsidePermimeter_IsFalse(float x, float y, string msg)
        {
            var c = CreateCircle(0, 0, 1);

            var actual = c.Contains(new Vector2(x, y));

            Assert.False(actual, msg);
        }

        [Theory]
        [InlineData(0, 0, "Rect at origin should intersect")]
        [InlineData(-0.5f, -0.5f, "Overlay on origin should intersect")]
        [InlineData(1, 0, "Perimeter should intersect left edge")]
        [InlineData(-2, 0, "Perimeter should intersect right edge")]
        [InlineData(0, -2, "Perimeter should intersect bottom edge")]
        [InlineData(0, 1, "Perimeter should intersect top edge")]
        public void Intersects_Rectangle_WithinPerimeter_IsTrue(float x, float y, string msg)
        {
            var c = CreateCircle(x: 0, y: 0, r: 1);
            var r = new Rectangle(x, y, width: 1, height: 1);

            var actual = c.Intersects(r);

            actual.Should().BeTrue(msg);
        }

        [Theory]
        [InlineData(0.75f, 0.75f, "Near perimeter does not intersect")]
        [InlineData(0, -2.25f, "Above perimeter does not intersect")]
        [InlineData(0, 1.25f, "Below perimeter does not intersect")]
        [InlineData(1.25f, 0, "Left perimeter does not intersect")]
        [InlineData(-2.25f, 0, "Right perimeter does not intersect")]
        public void Intersect_Rectangle_OutsidePerimeter_IsFalse(float x, float y, string msg)
        {
            var c = CreateCircle(x: 0, y: 0, r: 1);
            var r = new Rectangle(x, y, width: 1, height: 1);
            var actual = c.Intersects(r);

            Assert.False(actual, msg);
        }
    }
}
