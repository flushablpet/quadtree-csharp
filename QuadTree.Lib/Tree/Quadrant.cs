﻿namespace QuadTree.Lib.Tree;

public enum Quadrant
{
    UPPER_LEFT = 0, // NW
    UPPER_RIGHT = 1, // NE
    BOTTOM_LEFT = 2, // SW
    BOTTOM_RIGHT = 3 // SE
}
