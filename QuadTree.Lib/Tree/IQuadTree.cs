﻿using QuadTree.Lib.Shapes;
using System.Numerics;

namespace QuadTree.Lib.Tree;

public interface IQuadTree
{
    /// <summary>
    /// Bounding area for this node.
    /// </summary>
    IRectangle Bounds { get; init; }

    /// <summary>
    /// Number of data elements this node can hold before dividing into quads.
    /// </summary>
    int Capacity { get; }

    /// <summary>
    /// If this node has child subtrees.
    /// </summary>
    bool HasSubtrees { get; }

    /// <summary>
    /// Delete this node's subtrees.
    /// </summary>
    void Clear();

    /// <summary>
    /// Create subtrees on this node.
    /// </summary>
    void Divide();

    /// <summary>
    /// Insert a point into this node or its descendants.
    /// </summary>
    /// <param name="xy"></param>
    /// <returns></returns>
    bool Insert(Vector2 xy);

    /// <summary>
    /// Gets the vectors present in the given bounds.
    /// </summary>
    /// <param name="bounds"></param>
    /// <param name="points"></param>
    ICollection<Vector2> Query(Circle bounds, ICollection<Vector2> points);
}
