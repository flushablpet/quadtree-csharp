﻿using QuadTree.Lib.Shapes;
using System.Numerics;
using System.Linq;

namespace QuadTree.Lib.Tree;

/// <summary>
/// A QuadTree is a tree node structure with zero or four subtrees.
/// </summary>
public class QuadTree : IQuadTree
{
    /// A QuadTree can have 0 or 4 subtrees
    private const int DATA_CAPACITY = 4;
    private const int SUBTREE_CAPACITY = 4;
    private const uint MAX_TREE_DEPTH = 8;
    private readonly uint depth = default;

    private readonly List<IQuadTree> subtrees;
    private readonly List<Vector2> data;

    /// <summary>
    /// Creates a new instance of a QuadTree.
    /// </summary>
    /// <param name="bounds">Bounding area for the result.</param>
    /// <param name="depth">Depth position for the node. Default 0.</param>
    /// <returns></returns>
    public QuadTree(IRectangle bounds, uint depth = 0)
    {
        Bounds = bounds;
        this.depth = depth;
        subtrees = new(SUBTREE_CAPACITY);
        data = new(DATA_CAPACITY);
    }

    /// <inheritdoc/>
    public IRectangle Bounds { get; init; }

    /// <inheritdoc/>
    public bool HasSubtrees
    {
        get => subtrees.Count > 0;
    }

    /// <inheritdoc/>
    public int Capacity
    {
        get => data.Capacity;
    }

    public bool HasData
    {
        get => data.Any();
    }

    /// <inheritdoc/>
    public void Clear()
    {
        subtrees.Clear();
    }

    /// <inheritdoc/>
    public void Divide()
    {
        Clear();

        // Assumes node is not already divided
        var qs = (Quadrant[])Enum.GetValues(typeof(Quadrant));
        var subs = qs.Select(q => Bounds.Subdivide(q)).Select(b => new QuadTree(b, depth + 1));

        subtrees.AddRange(subs);

        foreach (var p in data)
        {
            var inserted =
                subtrees[0].Insert(p)
                || subtrees[1].Insert(p)
                || subtrees[2].Insert(p)
                || subtrees[3].Insert(p);
        }

        // Points have been moved down to child nodes.
        data.Clear();
    }

    /// <inheritdoc/>
    public bool Insert(Vector2 xy)
    {
        // The point doesn't fall in this Quad, exit
        if (!Bounds.Contains(xy))
        {
            return false;
        }

        if (!HasSubtrees)
        {
            // If we have space on this quad, add the element here.
            if (data.Count < DATA_CAPACITY || depth == MAX_TREE_DEPTH)
            {
                data.Add(xy);
                return true;
            }
            // Otherwise we need to split the quad and push elements down
            Divide();
        }

        // Recurse and exit when we find the correct subtree to insert into
        foreach (var subtree in subtrees)
        {
            if (subtree.Insert(xy))
            {
                return true;
            }
        }
        return false;
    }

    /// <inheritdoc/>
    public ICollection<Vector2> Query(Circle region, ICollection<Vector2>? found = default)
    {
        if (found is null)
            found = new List<Vector2>();

        if (!region.Intersects(Bounds))
            return found;

        if (HasSubtrees)
        {
            foreach (var subtree in subtrees)
            {
                subtree.Query(region, found);
            }
            return found;
        }

        foreach (var p in data)
        {
            if (region.Contains(p))
            {
                found.Add(p);
            }
        }

        return found;
    }
}
