﻿using QuadTree.Lib.Tree;
using System.Numerics;

namespace QuadTree.Lib.Shapes;

public sealed class Rectangle : Shape, IRectangle
{
    private readonly float halfWidth;
    private readonly float halfHeight;

    /// <summary>
    /// Create a new rectangle form. Origin at center.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <exception cref="ArgumentException"></exception>
    public Rectangle(float x, float y, float width, float height) : base(x, y)
    {
        if (width <= 0)
            throw new ArgumentOutOfRangeException(nameof(width));
        if (height <= 0)
            throw new ArgumentOutOfRangeException(nameof(height));

        Width = width;
        Height = height;
        halfWidth = width / 2;
        halfHeight = height / 2;
    }

    /// <inheritdoc/>
    public float Width { get; init; }

    /// <inheritdoc/>
    public float Height { get; init; }

    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public float Left
    {
        get => Origin.X - halfWidth;
    }

    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public float Right
    {
        get => Origin.X + halfWidth;
    }

    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public float Top
    {
        get => Origin.Y - halfHeight;
    }

    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public float Bottom
    {
        get => Origin.Y + halfHeight;
    }

    /// <summary>
    /// <inheritdoc/>
    /// True if arg's X is on or to right of Left edge and on or to left of Right edge,
    /// and arg's Y is on or below Top edge and on or above Bottom edge.
    /// </summary>
    /// <param name="point">2D point to test</param>
    /// <returns></returns>
    public override bool Contains(Vector2 point) =>
        point.X >= Left && point.X <= Right && point.Y >= Top && point.Y <= Bottom;

    /// <summary>
    /// <inheritdoc/>
    /// Tests for non-intersection, and returns the inverse.
    /// </summary>
    /// <param name="bounds">Region to test</param>
    /// <returns></returns>
    public override bool Intersects(IRectangle bounds) =>
        !(bounds.Left > Right || bounds.Right < Left || bounds.Top > Bottom || bounds.Bottom < Top);

    /// <inheritdoc/>
    public IRectangle Subdivide(Quadrant quadrant)
    {
        var (x1, y1) = quadrant switch
        {
            Quadrant.UPPER_LEFT => (Origin.X, Origin.Y),
            Quadrant.UPPER_RIGHT => (Origin.X + halfWidth, Origin.Y),
            Quadrant.BOTTOM_LEFT => (Origin.X, Origin.Y + halfHeight),
            Quadrant.BOTTOM_RIGHT => (Origin.X + halfWidth, Origin.Y + halfHeight),
            _ => throw new ArgumentOutOfRangeException(nameof(quadrant))
        };

        return new Rectangle(x1, y1, halfWidth, halfHeight);
    }
}
