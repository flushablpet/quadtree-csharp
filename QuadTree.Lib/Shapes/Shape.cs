﻿using System.Numerics;

namespace QuadTree.Lib.Shapes;

/// <summary>
/// A base class for things that want a corporeal form.
/// </summary>
public abstract class Shape : IIntersection, IDistance
{
    protected Shape(float x, float y) => Origin = new Vector2(x, y);

    /// <summary>
    /// 2D vector representing this object's origin on a plane.
    /// </summary>
    public Vector2 Origin { get; init; }

    /// <summary>
    /// Determines if this shape contains the given point.
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public abstract bool Contains(Vector2 point);

    /// <summary>
    /// Determines if this shape intersects the given bounding region.
    /// </summary>
    /// <param name="bounds"></param>
    /// <returns></returns>
    public abstract bool Intersects(IRectangle bounds);

    /// <summary>
    /// Calculate the distance between this shape's origin and the given point.
    /// </summary>
    /// <param name="point"></param>
    /// <returns>Distance scalar</returns>
    public float DistanceFrom(Vector2 point) => Vector2.Distance(Origin, point);

    /// <summary>
    /// Calculate the squared distance between this shape's origin and the given point.
    /// Avoids the sqrt operation of DistanceFrom.
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public float SquareDistanceFrom(Vector2 point) => Vector2.DistanceSquared(Origin, point);
}
