﻿using System.Numerics;

namespace QuadTree.Lib.Shapes;

/// <summary>
/// A rectangular bounding region.
/// </summary>
public interface IRectangle : IQuadrant, IIntersection, IDistance
{
    Vector2 Origin { get; }

    /// <summary>
    /// Full width of bounding area.
    /// </summary>
    float Width { get; init; }

    /// <summary>
    /// Full height of bounding area.
    /// </summary>
    float Height { get; init; }

    /// <summary>
    /// Left edge of bounding area.
    /// </summary>
    float Left { get; }

    /// <summary>
    /// Right edge of bounding area.
    /// </summary>
    float Right { get; }

    /// <summary>
    /// Top edge of bounding area.
    /// </summary>
    float Top { get; }

    /// <summary>
    /// Bottom edge of bounding area.
    /// </summary>
    float Bottom { get; }
}
