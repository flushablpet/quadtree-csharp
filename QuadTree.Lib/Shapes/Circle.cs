﻿using System.Numerics;

namespace QuadTree.Lib.Shapes;

public sealed class Circle : Shape, ICircle
{
    /// <summary>
    /// Create a new circle form. Origin at center point.
    /// </summary>
    /// <param name="x">Origin x</param>
    /// <param name="y">Origin y</param>
    /// <param name="radius">Circle radius</param>
    /// <exception cref="ArgumentException"></exception>
    public Circle(float x, float y, float radius) : base(x, y)
    {
        if (radius <= 0)
            throw new ArgumentException("Radius must be > 0", nameof(radius));

        Radius = radius;
        RadiusSq = Math.Pow(Radius, 2);
    }

    /// <inheritdoc/>
    public float Radius { get; init; }

    /// <inheritdoc/>
    public double RadiusSq { get; init; }

    /// <inheritdoc/>
    public override bool Contains(Vector2 point)
    {
        var distance = SquareDistanceFrom(point);
        return distance <= RadiusSq;
    }

    /// <inheritdoc/>
    public override bool Intersects(IRectangle bounds)
    {
        var dx = Origin.X - Math.Max(bounds.Origin.X, Math.Min(Origin.X, bounds.Right));
        var dy = Origin.Y - Math.Max(bounds.Origin.Y, Math.Min(Origin.Y, bounds.Bottom));

        return SquareDistanceFrom(new Vector2(dx, dy)) <= RadiusSq;
    }
}
