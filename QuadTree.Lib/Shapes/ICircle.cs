﻿using System.Numerics;

namespace QuadTree.Lib.Shapes;

/// <summary>
/// A circular bounding region.
/// </summary>
internal interface ICircle : IIntersection, IDistance
{
    /// <summary>
    /// 2D origin of this circle.
    /// </summary>
    Vector2 Origin { get; }

    /// <summary>
    /// Get the radius of this circle.
    /// </summary>
    float Radius { get; init; }

    /// <summary>
    /// Get the squared radius of this circle.
    /// </summary>
    double RadiusSq { get; init; }
}
