﻿using QuadTree.Lib.Tree;

namespace QuadTree.Lib.Shapes;

/// <summary>
/// Quadtree subdivision
/// </summary>
public interface IQuadrant
{
    /// <summary>
    /// Divides the rectangle and returns a new rectangle representing the given quadrant.
    /// </summary>
    /// <param name="quadrant"></param>
    /// <returns></returns>
    IRectangle Subdivide(Quadrant quadrant);
}
