﻿using System.Numerics;

namespace QuadTree.Lib.Shapes;

/// <summary>
/// Distance API methods.
/// </summary>
public interface IDistance
{
    /// <summary>
    /// Returns the distance value between this point origin and another.
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public float DistanceFrom(Vector2 other);

    /// <summary>
    /// Returns the squared distance value between this point origin and another.
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public float SquareDistanceFrom(Vector2 other);
}
