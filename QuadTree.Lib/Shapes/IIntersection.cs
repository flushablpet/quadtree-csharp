﻿using System.Numerics;

namespace QuadTree.Lib.Shapes;

/// <summary>
/// Intersection and containment API methods.
/// </summary>
public interface IIntersection
{
    /// <summary>
    /// Indicates if the given point arg is on or within the boundary of this shape.
    /// </summary>
    /// <param name="point">A 2D point</param>
    /// <returns></returns>
    public abstract bool Contains(Vector2 point);

    /// <summary>
    /// Indicates if the given bounds arg intersects with the boundary of this shape.
    /// </summary>
    /// <param name="bounds">Other bounds</param>
    /// <returns></returns>
    public abstract bool Intersects(IRectangle bounds);
}
